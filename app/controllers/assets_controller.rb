class AssetsController < ApplicationController

  helper :version

  def index
    @assets = Asset.paginate(:page => params[:page])
  end

  def show
    @asset = Asset.find(params[:id])
    @asset.build_operating_system if @asset.operating_system.blank?
    @asset.build_customer if @asset.customer.blank?
  end

end
