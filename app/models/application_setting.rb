class ApplicationSetting
  CONFIG_FILE = Rails.root.join('config', 'site_config.yml')

  class << self

    # Retrieve any configuration setting, or nil if not defined
    def [] key
      values[key.to_s]
    end

    def hostname
      values['hostname'] || 'confine.local'
    end

    def password_encryption_pepper
      values['password_encryption_pepper'].tap do |pepper|
        raise ArgumentError.new("No password_encryption_pepper set in #{CONFIG_FILE}") if pepper.blank?
      end
    end

    def reset_config!
      @overrides = nil
    end

    private
    def values
      @overrides ||= load_config
    end

    def load_config
      return Hash.new unless File.exist?(CONFIG_FILE)
      File.open(CONFIG_FILE) do |file|
        if result = YAML.load(file)
          return result
        else
          raise ArgumentError.new("Error loading #{CONFIG_FILE}")
        end
      end
    end
  end
end
