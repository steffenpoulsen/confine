class Asset < ActiveRecord::Base
  has_paper_trail
  include ActiveRecord::Diff
  diff :exclude => [:updated_at]

  belongs_to :customer
  belongs_to :operating_system
  has_many :ip_addresses, :dependent => :destroy

  accepts_nested_attributes_for :ip_addresses

  # Pagination setting
  def self.per_page
    25
  end

  # Given a full hash of data from a CMDB system, create a new Asset if any changes are found.
  # Note, the instance this is called on must match the +cmdb_id+ in the hash
  def create_from_cmdb_data data
    self.readonly!
    self.attributes = data
    raise ArgumentError.new("Cannot change cmdb_id") if self.changes.keys.include?('cmdb_id')
    Asset.create!(data) if self.changed_for_autosave?
    self.reload
  end

  def customer_attributes= attributes
    self.customer = Customer.locate_and_update_by_cmdb_id(attributes[:cmdb_id], attributes)
  end

  def operating_system_attributes= attributes
    self.operating_system = OperatingSystem.custom_find_or_create_by_name_and_os_version(attributes[:name], attributes[:os_version])
  end

end