class Customer < ActiveRecord::Base
  has_paper_trail

  has_many :assets

  def self.locate_and_update_by_cmdb_id cmdb_id, attributes
    if instance = self.where(:cmdb_id => cmdb_id).first
      instance.update_attributes!(attributes)
    else
      instance = create!(attributes)
    end
    instance
  end

end
