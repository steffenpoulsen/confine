class OperatingSystem < ActiveRecord::Base
  has_paper_trail

  validate :unique_lowercase_name_and_os_version

  has_many :assets

  scope :case_insensitive_match, lambda { |name, os_version|
    where('lower(name) = lower(?) AND lower(os_version) = lower(?)', name, os_version)
  }

  def self.custom_find_or_create_by_name_and_os_version name, os_version
    if instance = case_insensitive_match(name, os_version).first
      instance
    else
      create!(:name => name, :os_version => os_version)
    end
  end

  private
  def unique_lowercase_name_and_os_version
    unless self.class.case_insensitive_match(name, os_version).blank?
      errors.add(:name, 'Should be unique with version')
    end
  end

end
