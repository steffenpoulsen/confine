class User < ActiveRecord::Base

  devise :ldap_authenticatable, :trackable, :lockable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password
end
