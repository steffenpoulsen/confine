module VersionHelper
  def show_latest_history(object)
    render object.versions.reverse, :object => object
  end

  def display_difference_to_newer version, object
    differences = difference_to_newer(version, object)
    display_difference_hash(differences)
  end

  def difference_to_newer version, object
    if version.reify && version.next
      current_version = version.reify
      next_version    = version.next.reify
      current_version.diff(next_version)
    elsif version.reify
      version.reify.diff(object)
    else
      {}
    end
  end

  private

  def display_difference_hash differences
    out = {}
    differences.each_pair do |attribute, changes|
      out[attribute] = Differ.diff_by_word(changes.last, changes.first)
    end
    out
  end
end
