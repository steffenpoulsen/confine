require 'spec_helper'

describe Customer do
  it { should have_db_column(:name) }
  it { should have_db_column(:cmdb_id) }
  it { should have_many(:assets) }

  it { should have_paper_trail }

  it "should have a usable factory" do
    expect do
      2.times { subject.class.make }
    end.to change { subject.class.count }.by(2)
  end

end

describe Customer, '.locate_and_update_by_cmdb_id' do
  subject { Customer.locate_and_update_by_cmdb_id(cmdb_id, attributes) }
  let(:cmdb_id) { 55 }
  let(:attributes) {{
    :cmdb_id => cmdb_id,
    :name => customer_name
  }}
  let(:customer_name) { "Primary Customer" }

  context "a new customer" do
    it "should return the customer" do
      subject.should be_instance_of(Customer)
    end

    it "should create a record" do
      expect { subject }.to change { Customer.count }.by(1)
    end

    it "should have the expected cmdb_id" do
      subject.cmdb_id.should == cmdb_id
    end

    it "should have the expected name" do
      subject.name.should == customer_name
    end
  end

  context "a customer with the same cmdb_id exists" do
    let!(:customer) { Customer.make(:cmdb_id => cmdb_id) }

    it "should return the customer" do
      subject.should == customer
    end

    it "should not create a new record" do
      expect { subject }.to_not change { Customer.count }
    end

    it "should update the name" do
      expect { subject }.to change { customer.reload.name }.to(customer_name)
    end

    it "should log the change in the history" do
      expect { subject }.to change { customer.versions.count }.by(1)
    end

  end

  context "an identicial customer already exists" do
    let!(:customer) { Customer.make(:cmdb_id => cmdb_id, :name => customer_name) }

    it "should return the customer" do
      subject.should == customer
    end

    it "should not create a new record" do
      expect { subject }.to_not change { Customer.count }
    end

    it "should log no change in the history" do
      expect { subject }.to_not change { customer.versions.count }
    end
  end
end