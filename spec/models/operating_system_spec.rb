require 'spec_helper'

describe OperatingSystem do

  it { should have_db_column(:name) }
  it { should have_db_column(:os_version) }

  it { should have_many(:assets) }

  it { should have_paper_trail }

  describe "lowercase uniqueness" do
    let!(:existing_instance) { OperatingSystem.create!(
      :name       => name,
      :os_version => os_version
    )}
    let(:name) { 'Main OS' }
    let(:os_version) { "2010 SP2" }

    it "should allow a new instance with different values" do
      OperatingSystem.new(:name => 'Other OS', :os_version => '1999').should be_valid
    end

    it "should not validate even in rails" do
      instance = OperatingSystem.new(:name => name, :os_version => os_version)
      instance.should_not be_valid
      instance.errors[:name].should include('Should be unique with version')
    end

    it "should be backed by a unique index, so it works even if validations are disabled in rails" do
      instance = OperatingSystem.new(:name => name, :os_version => os_version)
      expect do
        instance.save(:validate => false)
      end.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end

  it "should have a usable factory" do
    expect do
      2.times { subject.class.make }
    end.to change { subject.class.count }.by(2)
  end

  it "should set version when given to factory" do
    OperatingSystem.make(:os_version => '1999').os_version.should == '1999'
  end
end
