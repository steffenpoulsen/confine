require 'spec_helper'

describe Asset do
  it { should have_db_column(:name) }
  it { should have_db_column(:fqdn) }
  it { should have_db_column(:role) }
  it { should have_db_column(:cmdb_id) }

  it { should belong_to(:customer) }
  it { should belong_to(:operating_system) }
  it { should have_many(:ip_addresses) }

  it { should have_paper_trail }

  it "should have a usable factory" do
    expect do
      2.times { subject.class.make }
    end.to change { subject.class.count }.by(2)
  end
end

describe Asset, "accept nested attributes" do
  let(:instance) { Asset.make_unsaved }
  subject { 
    instance.attributes = new_attributes;
    instance
  }

  describe ".ip_addresses_attributes=" do
    let(:new_attributes) {{
      :ip_addresses_attributes => [
        { :ip => '192.168.1.1' },
        { :ip => '192.168.1.10' },
      ]
    }}

    it "should accept a list of IPAddresses" do
      subject.ip_addresses.map(&:ip).should include_only(
        '192.168.1.1', '192.168.1.10'
      )
    end

    it "should create two new IPAddresses" do
      subject
      expect {
        instance.save!
      }.to change { IpAddress.count }.by(2)
    end

    it "should not automatically create a new version of the asset" do
      expect {
        subject.save!
      }.to_not change { instance.versions }
    end

    it "should not automatically create a new version of the asset when adding a new associated" do
      subject.save!
      expect {
        instance.ip_addresses.create!(:ip => '10.0.0.5')
      }.to_not change { subject.versions }
    end
  end

  describe ".customer=" do
    let(:new_attributes) {{
      :customer_attributes => {
        :name    => customer_name,
        :cmdb_id => customer_cmdb_id,
      }
    }}
    let(:customer_name) { 'Big Corp' }
    let(:customer_cmdb_id) { 34 }

    it "should create a new customer" do
      expect { subject.save! }.to change { Customer.count }.by(1)
    end

    it "should have the new customer name" do
      subject.customer.name.should == customer_name
    end
  end

  describe ".operating_system=" do
    let(:new_attributes) {{
      :operating_system_attributes => {
        :name       => os_name,
        :os_version => os_version
      }
    }}
    let(:os_name) { 'BeOS' }
    let(:os_version) { '1999' }

    it "should create a new operating_system" do
      expect { subject.save! }.to change { OperatingSystem.count }.by(1)
    end

    it "should have the name of the operating system" do
      subject.operating_system.name.should == os_name
    end

    context "the OS already exists" do
      let!(:os) { OperatingSystem.make(:name => os_name.downcase, :os_version => os_version) }

      it "should not create a new operating_system" do
        expect { subject.save! }.to_not change { OperatingSystem.count }
      end

      it "should assign the operating_system" do
        subject.operating_system.should == os
      end

      it "should have the name of the operating system which is already saved" do
        subject.operating_system.name.should == os_name.downcase
      end
    end
  end
end

describe Asset, "versioning of attribute changes" do
  subject { Asset.make }

  it "should have a version to begin with" do
    subject.versions.length.should == 1
  end

  it "should change version when an attribute changes" do
    subject.name = "All new"
    subject.save!
    subject.versions.length.should == 2
  end

end

describe Asset, "#create_from_cmdb_data" do
  let!(:instance) { Asset.make(
    :name    => asset_name,
    :cmdb_id => cmdb_id,
    :fqdn    => fqdn
  )}
  subject { instance.create_from_cmdb_data(cmdb_data) }
  let(:asset_name) { "Name From the Start" }
  let(:cmdb_id) { 77 }
  let(:fqdn) { 'my.local.host' }

  context "nothing changed in cmdb" do
    let(:cmdb_data) {{
      :name    => asset_name,
      :cmdb_id => cmdb_id
    }}

    it "should change the instance to be readonly" do
      subject
      instance.should be_readonly
    end

    it "should have no changes on the instance" do
      subject
      instance.should_not be_changed
    end

    it "should not create a new asset" do
      expect { subject }.to_not change { Asset.count }
    end

    it "should not create a new version for the existing asset" do
      expect { subject }.to_not change { Version.count }
    end
  end

  context "attemt update for an asset with a different cmdb_id" do
    let(:cmdb_data) {{
      :name    => new_asset_name,
      :cmdb_id => 90,
    }}
    let(:new_asset_name) { "brand new asset name" }

    it "should raise an error as the cmdb_id does not match" do
      expect { subject }.to raise_error(ArgumentError, /cmdb_id/)
    end
  end

  context "asset attribute has changed" do
    let(:cmdb_data) {{
      :name    => new_asset_name,
      :cmdb_id => cmdb_id,
      :fqdn    => fqdn 
    }}
    let(:new_asset_name) { "brand new asset name" }

    it "should create a new asset" do
      expect { subject }.to change { Asset.count }.by(1)
    end

    it "should set the name of the new asset" do
      subject
      Asset.last.name.should == new_asset_name
    end

    it "should set the cmdb_id of the new asset" do
      subject
      Asset.last.cmdb_id.should == cmdb_id
    end

    it "should not create a new version for the existing asset" do
      expect { subject }.to_not change { instance.versions.count }
    end
  end

  context "adding an ip address" do
    let(:cmdb_data) {{
      :cmdb_id                 => cmdb_id,
      :name                    => asset_name,
      :fqdn                    => fqdn,
      :ip_addresses_attributes => [
        :ip                    => new_ip
      ]
    }}
    let(:new_ip) { '10.10.10.6' }


    it "should create a new asset" do
      expect { subject }.to change { Asset.count }.by(1)
    end

    it "should create a new ip address" do
      expect { subject }.to change { IpAddress.count }.by(1)
    end

    it "should associate the ip address with the newly created asset" do
      subject
      Asset.last.ip_addresses.map(&:ip).should include_only(new_ip)
    end

    it "should not change the subject" do
      subject.changes.should == {}
    end

    it "should not change the subject associations" do
      subject
      instance.changed_for_autosave?.should == false
    end

  end
end
