require 'spec_helper'

describe IpAddress do
  it { should have_db_column(:interface_type) }
  it { should have_db_column(:ip) }
  it { should have_db_column(:fqdn) }
  it { should have_db_column(:asset_id) }

  it { should belong_to(:asset) }

  it "should have a usable factory" do
    expect do
      2.times { subject.class.make }
    end.to change { subject.class.count }.by(2)
  end

end
