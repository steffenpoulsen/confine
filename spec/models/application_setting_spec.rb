require 'spec_helper'

describe ApplicationSetting do
  before(:each) do
    ApplicationSetting.reset_config!
  end

  def with_config config_str 
    io = StringIO.new(config_str)
    mock(File).open(ApplicationSetting::CONFIG_FILE).at_least(1).yields(io)
  end

  describe ".[key]" do
    subject { ApplicationSetting[key] }
    let(:key) { :extra_setting }

    context "no value defined for setting" do
      it { should == nil }
    end

    context "value defined for setting" do
      it "should return the value" do
        with_config('extra_setting: bonus value')
        subject.should == 'bonus value'
      end
    end

    context "key is a string for a defined value" do
      let(:key) { 'extra_setting' }
      it "should return the value" do
        with_config('extra_setting: bonus value')
        subject.should == 'bonus value'
      end
    end

  end

  describe '.hostname' do
    subject { ApplicationSetting.hostname }

    it "should return a default usable hostname" do
      with_config('---')
      subject.should == 'confine.local'
    end

    it "should use any override from the config" do
      with_config('hostname: "custom.override.local"')
      subject.should == 'custom.override.local'
    end
  end

  describe ".password_encryption_pepper" do
    subject { ApplicationSetting.password_encryption_pepper }

    context "when no config file exist" do
      before(:each) do
        mock(File).exist?(ApplicationSetting::CONFIG_FILE).at_least(1) { false }
      end

      it "should raise an error when no pepper is set" do
        expect do
          subject
        end.to raise_error(/No password_encryption_pepper/i)
      end
    end

    context "with a config file exist" do
      before(:each) do
        mock(File).exist?(ApplicationSetting::CONFIG_FILE).at_least(1) { true }
      end

      it "should return a set pepper setting" do
        with_config({ "password_encryption_pepper" => "horse123" }.to_yaml)
        subject.should == "horse123"
      end

      it "should raise an error if no pepper setting" do
        with_config({}.to_yaml)
        expect do
          subject
        end.to raise_error(/No password_encryption_pepper/i)
      end
    end
  end
end