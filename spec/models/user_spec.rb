require 'spec_helper'

describe User, "columns" do
  it { should have_db_column(:email) }
  it { should have_db_column(:last_sign_in_at) }
  it { should have_db_column(:last_sign_in_ip) }
  it { should have_db_column(:current_sign_in_at) }
  it { should have_db_column(:current_sign_in_ip) }

  it "should only allow mass-assignment of specific attributes" do
    User.accessible_attributes.should include_only('email', 'password')
  end
end

describe User, "devise modules" do
  it "should support LDAP" do
    User.included_modules.should include(Devise::Models::LdapAuthenticatable)
  end

  [Devise::Models::Trackable, Devise::Models::Lockable, Devise::Models::Authenticatable].each do |devise_module|
    it "should support extra devise functionality from '#{devise_module}'" do
      User.included_modules.should include(devise_module)
    end
  end
end