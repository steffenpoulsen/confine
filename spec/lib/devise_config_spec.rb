require 'spec_helper'

describe Devise, "global config" do
  it "should have a usable mail-address" do
    Devise.mailer_sender.should include('authentication@')
  end

  it "should create users automatically" do
    Devise.ldap_create_user.should == true
  end
end