module NavigationHelpers
  # Put helper methods related to the paths in your application here.

  def homepage
    "/"
  end

  def assets_page
    '/assets'
  end

  def asset_page asset
    url_for(asset)
  end
end

RSpec.configuration.include NavigationHelpers, :type => :acceptance
