require 'capybara/rails'

module Steak::Capybara
  include Rack::Test::Methods
  include Capybara
  include ActionView::Helpers::UrlHelper

  def app
    ::Rails.application
  end
end
# RSpec.configure do |config|
#   # include modules in every acceptance spec
#   config., :type => :acceptance
#   config.include Capybara, :type => :acceptance
#   
# end
RSpec.configuration.include Steak::Capybara, :type => :integration
