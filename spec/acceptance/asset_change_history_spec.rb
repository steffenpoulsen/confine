require File.expand_path(File.dirname(__FILE__) + '/acceptance_helper')

describe "assets index acceptance", :type => :acceptance do
  let!(:asset) { Asset.make(
    :name => asset_name,
    :fqdn => asset_fqdn
  ) }
  let(:asset_name) { 'BLIIP123'}
  let(:asset_fqdn) { '123.full.customer.local' }

  it "should show the creation event of the asset" do
    visit asset_page(asset)
    page.should have_content("create Asset")
  end

  context "when the name has changed" do
    before(:each) do
      asset.update_attributes(:name => new_asset_name)
    end
    let(:new_asset_name) { 'Refreshed BLIIP123' }

    it "should show the creation event of the asset" do
      visit asset_page(asset)
      page.should have_content("update Asset")
    end

    it "should show the creation event of the asset" do
      visit asset_page(asset)
      page.should have_css('ins.differ', :text => 'Refreshed ')
    end
  end

end