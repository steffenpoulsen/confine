require File.expand_path(File.dirname(__FILE__) + '/acceptance_helper')

describe "assets index acceptance", :type => :acceptance do
  let!(:asset) { Asset.make(
    :name => asset_name,
    :fqdn => asset_fqdn
  ) }
  let(:asset_name) { 'BLIIP123'}
  let(:asset_fqdn) { '123.full.customer.local' }

  it "should show the name of the asset" do
    visit assets_page
    page.should have_content(asset_name)
  end

  it "should show the hostname of the asset" do
    visit assets_page
    page.should have_content(asset_fqdn)
  end

  it "should be possible to show the asset by clicking the name" do
    visit assets_page
    click_link asset_name
    page.should have_content(asset_name)
  end

end

describe "asset show page", :type => :acceptance do
  let(:asset) { Asset.make(
    :role => asset_role
  )}
  let(:asset_role) { 'Mostly idling' }
  let!(:ip_address) { IpAddress.make(:asset => asset) }

  it "should show the customer name" do
    visit asset_page(asset)
    page.should have_content(asset.customer.name)
  end

  it "should show the role" do
    visit asset_page(asset)
    page.should have_content(asset_role)
  end

  it "should show the ip address" do
    visit asset_page(asset)
    page.should have_content(ip_address.ip)
  end

  it "should show the ip fqdn address" do
    visit asset_page(asset)
    page.should have_content(ip_address.fqdn)
  end

  context "and an operating system is assigned" do
    let!(:operating_system) { asset.update_attribute(:operating_system, OperatingSystem.make(:name => 'Windows', :os_version => 'ME')) }

    it "should show the operating system" do
      visit asset_page(asset)
      page.should have_content('Windows')
    end

    it "should show the version" do
      visit asset_page(asset)
      page.should have_content('ME')
    end
  end

end