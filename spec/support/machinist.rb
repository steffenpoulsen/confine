# Load machinist blueprints
require File.expand_path(File.dirname(__FILE__) + "/blueprints")

RSpec.configure do |config|
  # For repeatable Machinist output
  config.before(:all)    { Sham.reset(:before_all)  }
  config.before(:each)   { Sham.reset(:before_each) }
end
