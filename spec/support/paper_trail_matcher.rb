RSpec::Matchers.define :have_paper_trail do
  match do |actual|
    actual.class.included_modules.should include(PaperTrail::Model::InstanceMethods)
  end
end

