require 'machinist/object'
require 'machinist/active_record'
require 'sham'
require 'faker'

Sham.asset_name { Faker::Internet.domain_word.upcase }
Sham.fqdn { Faker::Internet.domain_name }
Sham.name { Faker::Company.name }
Sham.ip { [3..25, 0..10, 0..10, 0..25].map(&:to_a).map(&:sample).join('.') }

Asset.blueprint do
  name { Sham.asset_name }
  fqdn
  customer
end

Customer.blueprint do
  name
end

OperatingSystem.blueprint do
  name
  os_version { Faker::Company.catch_phrase }
end

IpAddress.blueprint do
  interface_type { ['NAT', 'physical', 'virtual'].sample }
  asset
  ip
  fqdn { Faker::Internet.domain_name }
end