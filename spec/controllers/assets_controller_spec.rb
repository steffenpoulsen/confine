require 'spec_helper'

describe AssetsController do

  describe "GET 'index'" do
    it "should be successful" do
      get 'index'
      response.should be_success
    end

    it "should assign to @assets" do
      get 'index'
      assigns.should have_key(:assets)
    end

    it "should paginate" do
      get 'index'
      assigns[:assets].should respond_to(:total_pages)
    end
  end

  describe "GET 'show'" do
    let(:asset) { Asset.make }

    def do_get
      get :show, :id => asset.id
    end

    it "should succeed" do
      do_get
      response.should be_success
    end

    it "should assign to @asset" do
      do_get
      assigns[:asset].should == asset
    end
  end

end
