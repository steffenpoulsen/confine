desc 'Do what the test-server does after each push'
task :ci => [
  'setup',
  'db:migrate',
  'spec'
]
task :default => :ci

# rspec assumes db:test:prepare is the right thing to do, so we have to redefine the prerequisites
Rake::Task[:spec].clear_prerequisites

# using this as db:test:prepare does not copy the full structure
task :spec => [
  'db:abort_if_pending_migrations',
  'db:test:clone_structure'
]

