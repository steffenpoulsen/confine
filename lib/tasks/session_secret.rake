file 'config/initializers/secret_token.rb' do
  open(Rails.root.join('config', 'initializers', 'secret_token.rb'), 'w') do |file|
    file.write <<END
# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!
# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.

# This file MUST NOT be checked into version control, but the files 
# MUST be identical if multiple servers are used
Confine::Application.config.secret_token = "#{ActiveSupport::SecureRandom.hex(40)}"
END
  end
end
desc "Generate config/initializers/secret_token.rb secret"
task "session:secret" => 'config/initializers/secret_token.rb'