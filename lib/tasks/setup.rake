desc "Setup app, ensure dependencies are available"
task :setup => "session:secret"
task :setup => 'confine:copy_layout_assets'
task :setup => 'git:submodule:update'
task :setup => 'admin:copy_assets'

namespace :confine do
  task :copy_layout_assets do
    `rails generate confine_tdc_skin_theme:assets`
  end
end

namespace :git do
  namespace :submodule do
    task :init do
      `git submodule init`
    end
    task :update => :init do
      `git submodule update`
    end
  end
end
