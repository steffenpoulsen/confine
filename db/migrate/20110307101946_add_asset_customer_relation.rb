class AddAssetCustomerRelation < ActiveRecord::Migration
  def self.up
    add_column :assets, :customer_id, :integer
    add_foreign_key(:assets, :customers)
  end

  def self.down
    remove_foreign_key(:assets, :customers)
    remove_column :assets, :customer_id
  end
end