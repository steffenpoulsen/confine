class RenameAssetColumnsToSimplify < ActiveRecord::Migration
  def self.up
    rename_column :assets, :full_hostname, :fqdn
    rename_column :assets, :asset_name, :name
  end

  def self.down
    rename_column :assets, :name, :asset_name
    rename_column :assets, :fqdn, :full_hostname
  end
end