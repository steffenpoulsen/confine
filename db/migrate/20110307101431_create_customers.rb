class CreateCustomers < ActiveRecord::Migration
  def self.up
    create_table :customers do |t|
      t.string :name
      t.integer :cmdb_id

      t.timestamps
    end
    add_index :customers, :cmdb_id, :unique => true
  end

  def self.down
    remove_index :customers, :column => :cmdb_id
    drop_table :customers
  end
end