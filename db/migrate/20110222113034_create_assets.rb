class CreateAssets < ActiveRecord::Migration
  def self.up
    create_table :assets do |t|
      t.string :asset_name
      t.string :full_hostname
      t.timestamps
    end
    add_index :assets, [:asset_name], :unique => true
    add_index :assets, [:full_hostname], :unique => true
  end

  def self.down
    remove_index :assets, :column => [:asset_name]
    remove_index :assets, :column => [:full_hostname]
    drop_table :assets
  end
end