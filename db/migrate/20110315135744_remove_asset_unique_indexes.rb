class RemoveAssetUniqueIndexes < ActiveRecord::Migration
  def self.up
    remove_index :assets, :name => :index_assets_on_asset_name
    remove_index :assets, :name => :index_assets_on_full_hostname
  end

  def self.down
    add_index :assets, [:name], :unique => true, :name => 'index_assets_on_asset_name'
    add_index :assets, [:fqdn], :unique => true, :name => 'index_assets_on_full_hostname'
  end
end
