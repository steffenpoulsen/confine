class RenameIpAddressTypeColumn < ActiveRecord::Migration
  def self.up
    rename_column :ip_addresses, :type, :interface_type
  end

  def self.down
    rename_column :ip_addresses, :interface_type, :type
  end
end