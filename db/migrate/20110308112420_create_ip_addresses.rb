class CreateIpAddresses < ActiveRecord::Migration
  def self.up
    create_table :ip_addresses do |t|
      t.string :type
      t.string :fqdn
      t.column :ip, :inet
      t.integer :asset_id, :null => false

      t.timestamps
    end

    add_foreign_key(:ip_addresses, :assets)
  end

  def self.down
    drop_table :ip_addresses
  end
end
