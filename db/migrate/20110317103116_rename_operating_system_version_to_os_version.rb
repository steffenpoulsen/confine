class RenameOperatingSystemVersionToOsVersion < ActiveRecord::Migration
  def self.up
    rename_column :operating_systems, :version, :os_version
  end

  def self.down
    rename_column :operating_systems, :os_version, :version
  end
end