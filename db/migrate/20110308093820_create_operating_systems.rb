class CreateOperatingSystems < ActiveRecord::Migration
  def self.up
    create_table :operating_systems, :force => true do |t|
      t.string :name
      t.string :version
      t.timestamps
    end

    add_column :assets, :operating_system_id, :integer
    add_foreign_key(:assets, :operating_systems)

    execute <<-EOS
      CREATE UNIQUE INDEX operating_systems_name_title ON operating_systems (lower(name), lower(version))
    EOS
  end

  def self.down
    drop_table :operating_systems
  end
end