class AddCmdbIdToAsset < ActiveRecord::Migration
  def self.up
    add_column :assets, :cmdb_id, :integer
    add_index :assets, :cmdb_id
  end

  def self.down
    remove_index :assets, :cmdb_id
    remove_column :assets, :cmdb_id
  end
end