class AddRoleToAsset < ActiveRecord::Migration
  def self.up
    add_column :assets, :role, :string
  end

  def self.down
    remove_column :assets, :role
  end
end