RailsAdmin.authenticate_with {}

RailsAdmin.config do |config|
  config.excluded_models << User
  config.excluded_models << Asset
  config.excluded_models << Customer
  config.excluded_models << IpAddress
  config.excluded_models << OperatingSystem
end